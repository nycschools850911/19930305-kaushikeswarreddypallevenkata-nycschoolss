//
//  SchoolDetailsViewController.swift
//  19930305-KaushikeswarreddyPalleVenkata-NYCSchools
//
//  Created by Kaushik on 16/03/23.
//

import UIKit
import MapKit

class SchoolDetailsViewController: UIViewController {

    @IBOutlet weak var title_lbl: UILabel!
    @IBOutlet weak var address_lbl: UILabel!
    @IBOutlet weak var email_lbl: UILabel!
    @IBOutlet weak var mobile_btn: UIButton!
    
    @IBOutlet weak var readingScore_lbl: UILabel!
    @IBOutlet weak var writingScore_lbl: UILabel!
    @IBOutlet weak var mathScore_lbl: UILabel!
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var mapView: MKMapView!
    
    var schoolModel: NYCSchoolModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "School Details"
        // Do any additional setup after loading the view.
        guard let school = schoolModel else {return}
        showDetails(schoolModel: school)
        getSchoolSATScores()
        indicator.startAnimating()
    }
    
    func showDetails(schoolModel: NYCSchoolModel) {
        title_lbl.text = schoolModel.schoolName
        address_lbl.text = schoolModel.location
        email_lbl.text = "Email To:  \(schoolModel.schoolEmail ?? "")"
        mobile_btn.setTitle("Contact Us: \(schoolModel.phoneNumber ?? "")", for: .normal)
        
        let coord = CLLocationCoordinate2D(latitude: Double(schoolModel.latitude ?? "0.0") ?? 0.0, longitude: Double(schoolModel.longitude ?? "0.0") ?? 0.0)
        let anno = SchoolAnnotation(schoolId: schoolModel.dbn ?? "", coordinate: coord, title: schoolModel.schoolName ?? "", model: schoolModel)
        mapView.addAnnotation(anno)
        let mapCamera = MKMapCamera(lookingAtCenter: coord, fromEyeCoordinate: coord, eyeAltitude: 400.0)
        mapView.setCamera(mapCamera, animated: true)
    }

    @IBAction func callClicked(_ sender: Any) {
        if (UIApplication.shared.canOpenURL(URL(string: "tel://\(schoolModel?.phoneNumber ?? "")")!)) {
            UIApplication.shared.open(URL(string: "tel://\(schoolModel?.phoneNumber ?? "")")!)
        }
    }
    
    @IBAction func getDirection(_ sender: Any) {
    }
    
    private func getSchoolSATScores() {
        let url = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?$query=SELECT%0A%20%20%60dbn%60%2C%0A%20%20%60school_name%60%2C%0A%20%20%60num_of_sat_test_takers%60%2C%0A%20%20%60sat_critical_reading_avg_score%60%2C%0A%20%20%60sat_math_avg_score%60%2C%0A%20%20%60sat_writing_avg_score%60%0AWHERE%20%60dbn%60%20IN%20(%22\(schoolModel?.dbn ?? "")%22)"
        Task {
            do {
                let (data, _) = try await URLSession.shared.asyncData(from: URL(string: url)!)
                indicator.stopAnimating()
                indicator.isHidden = true
                guard let satScores = try? JSONDecoder().decode(SATScores.self, from: data).first else {
                    self.readingScore_lbl.text = "Average Reading Score: --"
                    self.writingScore_lbl.text = "Average Writing Score: --"
                    self.mathScore_lbl.text = "Average Maths Score: --"
                    return
                }
                self.readingScore_lbl.text = "Average Reading Score: \(satScores.satCriticalReadingAvgScore ?? "")"
                self.writingScore_lbl.text = "Average Writing Score: \(satScores.satWritingAvgScore ?? "")"
                self.mathScore_lbl.text = "Average Maths Score: \(satScores.satMathAvgScore ?? "")"
                return
            } catch {
                throw(error)
            }
        }
    }
}
