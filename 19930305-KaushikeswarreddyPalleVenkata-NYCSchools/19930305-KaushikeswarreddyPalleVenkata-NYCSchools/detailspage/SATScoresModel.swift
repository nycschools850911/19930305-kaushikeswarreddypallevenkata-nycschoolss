//
//  SATScoresModel.swift
//  19930305-KaushikeswarreddyPalleVenkata-NYCSchools
//
//  Created by Kaushik on 17/03/23.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let sATScores = try? JSONDecoder().decode(SATScores.self, from: jsonData)

import Foundation

// MARK: - SATScore
struct SATScore: Codable {
    let dbn, schoolName, numOfSatTestTakers, satCriticalReadingAvgScore: String?
    let satMathAvgScore, satWritingAvgScore: String?

    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case numOfSatTestTakers = "num_of_sat_test_takers"
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satWritingAvgScore = "sat_writing_avg_score"
    }
}

typealias SATScores = [SATScore]
