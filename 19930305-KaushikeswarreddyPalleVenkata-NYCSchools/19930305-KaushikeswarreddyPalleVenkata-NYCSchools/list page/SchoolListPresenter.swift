//
//  SchoolListPresenter.swift
//  19930305-KaushikeswarreddyPalleVenkata-NYCSchools
//
//  Created by Kaushik on 17/03/23.
//

import Foundation

protocol SchoolListPresenterDelegate: AnyObject {
    func showSchools(schools: [NYCSchoolModel])
    func showErrorMessage(message: String)
    func showSchoolDetails(school: NYCSchoolModel)
    func navigateToDetailsPage(school: NYCSchoolModel)
}

protocol SchoolListPresenterProtocol: SchoolListInteractorDelegate {
    var delegate: SchoolListPresenterDelegate? {get set}
}

// MARK: interactor delegate methods
class SecurityQuestionsPresenter: SchoolListPresenterProtocol {
    public var delegate: SchoolListPresenterDelegate?
    
    public func didGetSchoolsList(schools: [NYCSchoolModel]) {
        delegate?.showSchools(schools: schools)
    }
    
    public func showFailureToGetSchoolsList(message: String) {
        delegate?.showErrorMessage(message: message)
    }
    
    public func didShowSchoolDetails(schoolDetail: NYCSchoolModel) {
        delegate?.showSchoolDetails(school: schoolDetail)
    }
    
    public func didNavigateToDetailsPage(schoolDetail: NYCSchoolModel) {
        delegate?.navigateToDetailsPage(school: schoolDetail)
    }
    
}
