//
//  SchoolsListViewController.swift
//  19930305-KaushikeswarreddyPalleVenkata-NYCSchools
//
//  Created by Kaushik on 17/03/23.
//

import UIKit
import MapKit

class SchoolsListViewController: UIViewController {
    
    private var interactor: SchoolListInteractorProtocol!
    private var presenter: SchoolListPresenterProtocol!
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var detailsView: UIView!
    
    @IBOutlet weak var title_lbl: UILabel!
    @IBOutlet weak var subtitle_lbl: UILabel!
    @IBOutlet weak var phNum_lbl: UILabel!
    @IBOutlet weak var email_lbl: UILabel!
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    private var isCameraSet = false
    private var selectedSchool: NYCSchoolModel!
    
    override func viewDidLoad()   {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = "NYC Schools"
        presenter = SecurityQuestionsPresenter()
        interactor = SchoolListInteractor(delegate: presenter)
        presenter.delegate = self
        Task {
            indicator.startAnimating()
            await interactor.fetchSchoolsList()
        }
        mapView.delegate = self
        detailsView.isHidden = true
    }
    
    private func addAnnotations(schools: [NYCSchoolModel]) {
        for school in schools {
            let coord = CLLocationCoordinate2D(latitude: Double(school.latitude ?? "0.0") ?? 0.0, longitude: Double(school.longitude ?? "0.0") ?? 0.0)
            let anno = SchoolAnnotation(schoolId: school.dbn ?? "", coordinate: coord, title: school.schoolName ?? "", model: school)
            mapView.addAnnotation(anno)
            if !isCameraSet {
                let mapCamera = MKMapCamera(lookingAtCenter: coord, fromEyeCoordinate: coord, eyeAltitude: 4000.0)
                mapView.setCamera(mapCamera, animated: true)
                isCameraSet = true
            }
        }
        
    }
    
    @IBAction func navigate_details(_ sender: UIButton) {
        interactor.navigateToDetails(selectedSchool: selectedSchool)
    }
}

extension SchoolsListViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil;
        } else {
            let pinIdent = "Pin";
            var pinView: MKMarkerAnnotationView;
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: pinIdent) as? MKMarkerAnnotationView {
                dequeuedView.annotation = annotation
                pinView = dequeuedView
            } else {
                pinView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: pinIdent)
            }
            return pinView;
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let annot = view.annotation as? SchoolAnnotation else { return }
        selectedSchool = annot.model
        interactor.showSchoolDetails(schoolDetail: annot.model!)
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        detailsView.isHidden = true
        selectedSchool = nil
    }
}

extension SchoolsListViewController: SchoolListPresenterDelegate {
    func showSchoolDetails(school: NYCSchoolModel) {
        detailsView.isHidden = false
        title_lbl.text = school.schoolName
        subtitle_lbl.text = school.location
        phNum_lbl.text = "Ph No: \(school.phoneNumber ?? "")"
        email_lbl.text = "Email: \(school.schoolEmail ?? "")"
    }
    
    func navigateToDetailsPage(school: NYCSchoolModel) {
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "SchoolDetailsViewController") as? SchoolDetailsViewController else { return }
        vc.schoolModel = school
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showSchools(schools: [NYCSchoolModel]) {
        
        addAnnotations(schools: schools)
        indicator.stopAnimating()
        indicator.isHidden = true
    }
    
    func showErrorMessage(message: String) {
        indicator.stopAnimating()
    }
    
}
