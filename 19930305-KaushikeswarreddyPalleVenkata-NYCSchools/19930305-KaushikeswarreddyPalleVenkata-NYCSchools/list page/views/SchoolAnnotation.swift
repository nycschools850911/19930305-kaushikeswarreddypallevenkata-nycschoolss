//
//  SchoolAnnotation.swift
//  19930305-KaushikeswarreddyPalleVenkata-NYCSchools
//
//  Created by Kaushik on 16/03/23.
//

import MapKit

class SchoolAnnotation: NSObject, MKAnnotation {
    let schoolId: String
    let coordinate: CLLocationCoordinate2D
    let title: String?
    var model: NYCSchoolModel?

    init(schoolId: String, coordinate: CLLocationCoordinate2D, title: String, model: NYCSchoolModel) {
         self.schoolId = schoolId
         self.coordinate = coordinate
         self.title = title
         self.model = model
    }
}
