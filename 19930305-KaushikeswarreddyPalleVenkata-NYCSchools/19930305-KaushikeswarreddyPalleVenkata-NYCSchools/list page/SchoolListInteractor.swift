//
//  SchoolListInteractor.swift
//  19930305-KaushikeswarreddyPalleVenkata-NYCSchools
//
//  Created by Kaushik on 17/03/23.
//

import Foundation

protocol SchoolListInteractorProtocol {
    func fetchSchoolsList() async
    func navigateToDetails(selectedSchool: NYCSchoolModel)
    func showSchoolDetails(schoolDetail: NYCSchoolModel)
}

protocol SchoolListInteractorDelegate: AnyObject {
    func didGetSchoolsList(schools: [NYCSchoolModel])
    func showFailureToGetSchoolsList(message: String)
    func didShowSchoolDetails(schoolDetail: NYCSchoolModel)
    func didNavigateToDetailsPage(schoolDetail: NYCSchoolModel)
}

final class SchoolListInteractor: SchoolListInteractorProtocol {
    public var delegate: SchoolListInteractorDelegate?
    private var worker: SchoolsListWorker!
    private var list: [NYCSchoolModel] = []
    
    init(delegate: SchoolListInteractorDelegate? = nil) {
        self.delegate = delegate
        self.worker = SchoolsListWorker()
    }
    
    func fetchSchoolsList() async {
        do {
            let schoolsList = try await worker.fetchSchoolList()
            print(schoolsList.count)
            list = schoolsList
            delegate?.didGetSchoolsList(schools: schoolsList)
        } catch {
            print(error)
        }
    }
    
    func navigateToDetails(selectedSchool: NYCSchoolModel) {
        delegate?.didNavigateToDetailsPage(schoolDetail: selectedSchool)
    }
    
    func showSchoolDetails(schoolDetail: NYCSchoolModel) {
        delegate?.didShowSchoolDetails(schoolDetail: schoolDetail)
    }
}
