//
//  SchoolListWorker.swift
//  19930305-KaushikeswarreddyPalleVenkata-NYCSchools
//
//  Created by Kaushik on 17/03/23.
//

import Foundation

class SchoolsListWorker {
    func fetchSchoolList() async throws -> [NYCSchoolModel]{
        do {
            let (data, _) = try await URLSession.shared.asyncData(from: URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")!)
            guard let nYCSchools = try? JSONDecoder().decode(NYCSchoolsList.self, from: data) else {
                return []
            }
            return nYCSchools
        } catch {
            throw(error)
        }
    }
}
